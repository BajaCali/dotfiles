;; Early init

; emacs frames does not have title bards and are rounded
(add-to-list 'default-frame-alist '(undecorated-round . t))

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips

(setq frame-resize-pixelwise t)

(package-initialize)

;; theme
; sync with system appereance settings

(defun my/apply-theme (appearance)
  "Load theme, taking current system APPEARANCE into consideration."
  (mapc #'disable-theme custom-enabled-themes)
  (pcase appearance
    ('light (load-theme 'gruvbox-light-soft t))
    ('dark (load-theme 'gruvbox-dark-hard t))))

; hook for keeping dark mode consistent with system's
(add-hook 'ns-system-appearance-change-functions #'my/apply-theme)

; apply theme now
(my/apply-theme ns-system-appearance)

; Prevent straigt.el from loading packages prior early init
(setq package-enable-at-startup nil)
