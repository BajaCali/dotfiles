; start with blank screen / *scratch* buffer
(setq inhibit-startup-message 1)

;(set-face-attribute 'default t :font "SourceCodePro+Powerline+Awesome Regular")

;; Make ESC quit prompts
; by default it's <ESC> <ESC> <ESC>
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; map cmd to emacs' meta key
(setq mac-command-modifier 'meta)

;; set fringe (side columns) to minimal width
(setq fringe-mode "minimal")

;; stop creating ~ files
(setq make-backup-files nil)

;; turn off bell sounds
(setq ring-bell-function 'ignore)

;; kill whole line (including its lewline) with C-k if done from begging of line
(setq kill-whole-line t)

(when (string= system-type "darwin")
  (setq dired-use-ls-dired nil))

;; show relative number on the left side of editor
(setq display-line-numbers-type 'relative)
(column-number-mode)
(global-display-line-numbers-mode)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                markdown-mode-hook
                ;; treemacs-mode-hook
                dired-mode
                dirvish-override-dired
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0)))
)

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("nongnu" . "https://elpa.nongnu.org/nongnu/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Done in early init
;; (package-initialize)

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)

(setq use-package-always-ensure t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage)
  )

(straight-use-package 'use-package)

(set-frame-parameter (selected-frame) 'alpha-background 85)
(add-to-list 'default-frame-alist '(alpha-background . 85))

(defalias 'yes-or-no-p 'y-or-n-p)

(customize-set-variable 'split-width-threshold 128)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq-default indent-tabs-mode nil)

(defun efs/org-babel-tangle-config ()
  (when (string-equal (buffer-file-name)
		      (expand-file-name "~/.dotfiles/.config/emacs/README.org"))

    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'efs/org-babel-tangle-config)))

(use-package super-save
  :ensure t
  :config
  (super-save-mode 1)
  (setq auto-save-default nil)
  (setq super-save-auto-save-when-idle t)
  )

(add-hook 'dired-mode-hook 'dired-hide-details-mode)

(customize-set-variable 'delete-by-moving-to-trash t)

(use-package gruvbox-theme)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("3e374bb5eb46eb59dbd92578cae54b16de138bc2e8a31a2451bf6fdb0f3fd81b" default))
 '(package-selected-packages '(magit gruvbox-theme)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(use-package exec-path-from-shell)

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

(when (daemonp)
  (exec-path-from-shell-initialize))

(use-package nerd-icons
  :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  (nerd-icons-font-family "Symbols Nerd Font Mono")
  )

;; Use `all-the-icons' as Dirvish's icon backend
(use-package all-the-icons)

(use-package diminish)

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)))


(use-package ivy
  :diminish
  :bind (:map ivy-minibuffer-map
	("TAB" . ivy-alt-done))
  :init
  (ivy-mode 1)
  (setq find-program "fd")
  )

; Accessible buffer switcher
(global-set-key (kbd "C-M-j") 'counsel-switch-buffer)

(use-package doom-modeline
 :ensure t
 :custom (doom-modeline-height 20)
 :init (doom-modeline-mode 1))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :custom
  (which-key-idle-delay 0.3)
  )

(use-package ivy-rich
  :after counsel
  :init
  (ivy-rich-mode 1)
  )

(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :config
    (setq ivy-initial-inputs-alist nil)) ; Don't start searches with ^

(use-package helpful
:custom
(counsel-describe-function-function #'helpful-callable)
(counsel-describe-variable-function #'helpful-variable)
:bind
([remap describe-function] . counsel-describe-function)
([remap describe-command] . helpful-command)
([remap describe-variable] . counsel-describe-variable)
([remap describe-key] . helpful-key))

(use-package general
  :config
  (general-define-key "C-M-j" 'counsel-switch-buffer))

;; (use-package dirvish
;;   :init
;;   (dirvish-override-dired-mode)
;;   :custom
;;   (dirvish-quick-access-entries ; It's a custom option, `setq' won't work
;;    '(("~" "~/"                          "Home")
;;      ("d" "~/Downloads/"                "Downloads")
;;      ("p" "~/Developer/"                "Developer")))
;;   (dirvish-mode-line-height 20)
;;   (dirvish-header-line-height 20)
;;   :config
;;   (setq insert-directory-program "gls")

;;   ;; (dirvish-peek-mode) ; Preview files in minibuffer
;;   ;; (dirvish-side-follow-mode) ; similar to `treemacs-follow-mode'
;;   (setq dirvish-mode-line-format
;;         '(:left (sort symlink) :right (omit yank index)))
;;   (setq dirvish-attributes
;;         '(all-the-icons file-time file-size collapse subtree-state vc-state )) ;git-mgs
;;   (setq delete-by-moving-to-trash t)
;;   (setq dired-listing-switches
;;         "-l --almost-all --human-readable --group-directories-first --no-group")
;;   (setq dirvish-enable-preview t)
;;   :bind ; Bind `dirvish|dirvish-side|dirvish-dwim' as you see fit
;;   (("C-c f" . dirvish)
;;    :map dirvish-mode-map ; Dirvish inherits `dired-mode-map'
;;    ("a"   . dirvish-quick-access)
;;    ("f"   . dirvish-file-info-menu)
;;    ("y"   . dirvish-yank-menu)
;;    ("N"   . dirvish-narrow)
;;    ("^"   . dirvish-history-last)
;;    ("h"   . dirvish-history-jump) ; remapped `describe-mode'
;;    ("s"   . dirvish-quicksort)    ; remapped `dired-sort-toggle-or-edit'
;;    ("v"   . dirvish-vc-menu)      ; remapped `dired-view-file'
;;    ("TAB" . dirvish-subtree-toggle)
;;    ("M-f" . dirvish-history-go-forward)
;;    ("M-b" . dirvish-history-go-backward)
;;    ("M-l" . dirvish-ls-switches-menu)
;;    ("M-m" . dirvish-mark-menu)
;;    ("M-t" . dirvish-layout-toggle)
;;    ("M-s" . dirvish-setup-menu)
;;    ("M-e" . dirvish-emerge-menu)
;;    ("M-j" . dirvish-fd-jump))
;;   )

(use-package copilot
  :straight (:host github :repo "zerolfx/copilot.el" :files ("dist" "*.el"))
  :ensure t
  :bind (:map copilot-completion-map
            ("<tab>" . 'copilot-accept-completion)
            ("TAB" . 'copilot-accept-completion)
            ("C-TAB" . 'copilot-accept-completion-by-word)
            ("C-<tab>" . 'copilot-accept-completion-by-word))
  )

;; (use-package lsp-grammarly
;; :ensure t
;; :hook (text-mode . (lambda ()
;;                      (require 'lsp-grammarly)
;;                      (lsp-deferred)))
;; )
                                       ; or lsp-deferred
;; (use-package grammarly
;;   :config
;;   (setq grammarly-username "xnemec6@fi.muni.cz")  ; Your Grammarly Username
;;   (setq grammarly-password "hypocrite-constrict-saffron")  ; Your Grammarly Password
;;   )

;; (use-package lsp-grammarly
;; :hook (text-mode . (lambda ()
;;                      (require 'lsp-grammarly)
;;                      (lsp-deferred))) ; or lsp-deferred
;; )

(use-package chatgpt
  :straight (:host github :repo "joshcho/ChatGPT.el" :files ("dist" "*.el"))
  )

(use-package plantuml-mode
    :custom
    (plantuml-jar-path "~/.config/emacs/bin/plantuml.jar")
    ; use M-x plantuml-download-jar to update PlantUML
    :config
    (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
    (add-to-list 'auto-mode-alist '("\\.puml\\'" . plantuml-mode))
    (setq plantuml-output-type "svg") ; or "svg" or "png" or "txt"
    :custom
    (plantuml-default-exec-mode 'jar)
)

  ;(setq org-plantuml-jar-path (expand-file-name "~/.config/emacs/bin/plantuml.jar"))
  ;(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
  ;(org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))

(setq org-plantuml-jar-path "~/.config/emacs/bin/plantuml.jar")

(setq org-startup-with-inline-images t)

(setq org-confirm-babel-evaluate nil)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((plantuml . t))) ; this line activates plantuml

(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
  (evil-set-undo-system 'undo-redo)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal)

  ;; Quickly change windows
  (global-unset-key (kbd "M-k"))
  (global-set-key (kbd "M-k") 'evil-window-next)

  ;; Exhcnage ^ <-> 0
  (evil-global-set-key 'motion "0" 'evil-first-non-blank)
  (evil-global-set-key 'motion "^" 'evil-beginning-of-line)

  ;; Replace "M-v" with evil paste
  (global-unset-key (kbd "M-v"))

  ;; Allow comand v to paste
  (define-key evil-insert-state-map (kbd "M-v") 'evil-paste-after)
  (define-key evil-motion-state-map (kbd "M-v") 'evil-paste-after)
  (define-key evil-window-map (kbd "q") 'evil-delete-buffer)
  )

;; Unset moving headlines for quickly chaning windows
(add-hook 'org-mode-hook (lambda ()
   (define-key outline-mode-map (kbd "<normal-state> M-k") nil)
   (define-key outline-mode-map (kbd "<normal-state> M-j") nil)
   ))

(use-package evil-collection
  :after evil
  :after magit
  :config
  (evil-collection-init))

(defun org/setup-headline-sizes ()
  (dolist (face '((org-level-1 . 1.4)
                  (org-level-2 . 1.25)
                  (org-level-3 . 1.15)
                  (org-level-4 . 1.05)
                  (org-level-5 . 1.05)
                  (org-level-6 . 1.05)
                  (org-level-7 . 1.05)
                  (org-level-8 . 1.05)))
    (set-face-attribute (car face) nil :weight 'bold :height (cdr face))
    )
  )

(defun efs/org-mode-setup ()
  (org-indent-mode)
  (visual-line-mode)
  (org/setup-headline-sizes)
  )

(use-package org
  :hook (org-mode . efs/org-mode-setup)
  :config
  (setq org-ellipsis " \uf460") ; a chevron
  )

;; Show hidden emphasis markers
(use-package org-appear
  :hook (org-mode . org-appear-mode)
  :custom
  (org-hide-emphasis-markers t)
  (org-appear-autolinks t)
  )

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '(" " "" "" "" "" "" "" "" "" "" "" ""))
  )

;; Set faces for heading levels
(with-eval-after-load "org"
  (dolist (face '((org-level-1 . 1.4)
                  (org-level-2 . 1.25)
                  (org-level-3 . 1.15)
                  (org-level-4 . 1.05)
                  (org-level-5 . 1.05)
                  (org-level-6 . 1.05)
                  (org-level-7 . 1.05)
                  (org-level-8 . 1.05)))
    (set-face-attribute (car face) nil :weight 'bold :height (cdr face))
    )
  )

;; Can't setup with :custom and hook to enable the mode, it just loops

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 88
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

(use-package toc-org)

(if (require 'toc-org nil t)
  (progn
    (add-hook 'org-mode-hook 'toc-org-mode)

    ;; enable in markdown, ftoo
    ; (add-hook 'markdown-mode-hook 'toc-org-mode)
    ; (define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point)
  )
(warn "toc-org not found"))

(setenv "PATH" (concat (getenv "PATH") ":/Library/TeX/texbin/"))

(with-eval-after-load 'ox-latex
   (add-to-list 'org-latex-classes
		'("fja"
		  "\\documentclass{fja}"
		  ("\\chapter{%s}" . "\\chapter*{%s}")
		  ("\\section{%s}" . "\\section*{%s}")
		  ("\\subsection{%s}" . "\\subsection*{%s}")
		  ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("fithesis4"
                 "\\documentclass{fithesis4}"
                 ("\\chapter{%s}" . "\\chapter{%s}")
                 ("\\section{%s}" . "\\section{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection{%s}")
))
)

;(use-package platuml-mode)

;(setq org-plantuml-jar-path (expand-file-name "~/.config/emacs/bin/plantuml.jar"))
;(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
;(org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))

(use-package org-roam
  :ensure t
  :config
  (org-roam-setup)
  (require 'org-roam-dailies) ;; Ensure the keymap is available
  (org-roam-db-autosync-mode)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i"    . completion-at-point)
         :map org-roam-dailies-map
         ("Y" . org-roam-dailies-capture-yesterday)
         ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :custom
  (org-roam-directory "~/Documents/org-roam")
  (org-roam-dailies-directory "journal/")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "%?"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)

     ("l" "programming language" plain
      "* Characteristics\n\n- Family: %?\n- Inspired by: \n\n* Reference\n\n"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)

     ("t" "talk" plain
      "\n* Basic Info\n\nSpeaker: %^{Speaker}\nTitle: ${title}\nDate: %<%Y/%m/%d>\nPlace: %^{Place}\nEvent: $^{Event}\n\n* Summary\n\n%?\n\n* Notes, Comments & Thoughts from the talk"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)

     ("b" "book notes" plain
      "\n* Basic Info\n\nAuthor: %^{Author}\nTitle: ${title}\nYear: %^{Year}\nRead: %<%Y/%m>\nWhere to find: %^{Where to find?}\n\n* Summary\n\n%?\n\n* Notes, Comments & Thoughts from the book"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
      :unnarrowed t)
     ))
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  )

(use-package org-ai
  :ensure t
  :commands (org-ai-mode
             org-ai-global-mode)
  :init
  (setq org-ai-openai-api-token
        "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")
  (add-hook 'org-mode-hook #'org-ai-mode) ; enable org-ai in org-mode
  (org-ai-global-mode) ; installs global keybindings on C-c M-a
  :config

  ;; (setq org-ai-default-chat-model "gpt-4") ; if you are on the gpt-4 beta:
  (setq org-ai-default-chat-model "gpt-3.5-turbo")
  ;; (org-ai-install-yasnippets) ; if you are using yasnippet and want `ai` snippets
  )

;; (use-package citar
;; :no-require
;; :custom
;; (org-cite-global-bibliography '("~/Developer/bakalarka/text/bibliography.bib"))
;; (org-cite-insert-processor 'citar)
;; (org-cite-follow-processor 'citar)
;; (org-cite-activate-processor 'citar)
;; (citar-bibliography org-cite-global-bibliography)
;; ;; optional: org-cite-insert is also bound to C-c C-x C-@
;; :bind
;; (:map org-mode-map :package org ("C-c b" . #'org-cite-insert))
;; )

(setq org-latex-pdf-process
      '("latexmk -pdflatex='pdflatex -interaction nonstopmode' -pdf -bibtex -f %f")
      )

(use-package org-contrib
    ;; :straight (:host github :repo "bzg/org-mode" :files (:defaults "lisp/*.el" ))
    :config
    (require 'ox-extra)
    (ox-extras-activate '(ignore-headlines))
    )

;;   (use-package org-contrib
;;   :ensure t)
;; (require 'ox-extra) ;; the package I wanted to include in my config
;; ;; and a function to activate the features of this package:
;; (ox-extras-activate '(latex-header-blocks ignore-headlines))

(add-to-list 'org-latex-classes
                   '("uni_template"
                   " \\documentclass[]{fithesis4}"))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom
  (projectile-completion-system 'ivy)
  :bind-keymap
  ("M-p" . projectile-command-map)
  :init
  (setq projectile-project-search-path '("~/Developer" ("~/Documents/school" . 3)))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :config
  (add-hook 'after-save-hook 'magit-after-save-refresh-status t)
  )

(eval-after-load 'smerge-mode
(lambda ()
  (define-key smerge-mode-map (kbd "C-c v") smerge-basic-map)
  (define-key smerge-mode-map (kbd "C-c C-v") smerge-basic-map)))

(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; (use-package tree-sitter
;;   :config
;;   (global-tree-sitter-mode t)
;;   )

;; (use-package tree-sitter-langs
;;   :after tree-sitter
;;   )

(use-package origami)

(use-package lsp-origami
  :after (lsp-mode origami)
  :config
  (add-hook 'lsp-after-open-hook #'lsp-origami-try-enable)
 )

(use-package yasnippet
  :hook (prog-mode . yas-minor-mode)
  )

(defun efs/lsp-mode-setup ()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))


(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (lsp-mode . efs/lsp-mode-setup)
  :hook (before-save-hook . lsp-format-buffer)
  :after flycheck
  :init
  ;; ( (setq lsp-keymap-prefix "C-c l") lsp-keymap-prefix "C-c l")
  ;; Or 'C-c l' 'C-l', 's-l'
  (setq lsp-keymap-prefix "C-c l")
  :config
  (lsp-enable-which-key-integration t)
  :custom
  (define-key global-map (kbd "M-l") nil)
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-inlay-hint-enable t)
  ;; (lsp-rust-analyzer-cargo-watch-command " --pedantic")
  ;; (lsp-rust-clippy-preference "on")
  ;; (lsp-enable-snippet nil)
  )

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'at-point)
  ;; (lsp-ui-sideline-show-hover t)
  ;; (lsp-ui-sideline-update-mode "line")
  ;; (lsp-ui-sideline-show-diagnostics t)
  ;; (lsp-ui-sideline-show-code-actions t)
  )

(use-package lsp-ivy)

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
	      ("<tab>" . company-complete-selection))
  (:map lsp-mode-map
	("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package markdown-mode
  :config
  (define-key markdown-mode-map (kbd "M-p") nil)
  )

(use-package rustic
  :after lsp-mode
  :hook (rustic-mode . lsp-deferred)
  :init
  (setq rustic-clippy-arguments "--pedantic")
  :custom
  (rustic-analyzer-command '("rustup" "run" "stable" "rust-analyzer"))
  )

(use-package swift-mode
  :hook (swift-mode . lsp-deferred)
  )

(use-package lsp-sourcekit
  :after lsp-mode
  :config
  (setq lsp-sourcekit-executable "/usr/bin/sourcekit-lsp")
  )

(use-package swift-playground-mode
  :straight (:host gitlab
                   :repo "michael.sanders/swift-playground-mode"
                   )
  :defer t
  :init
  (autoload 'swift-playground-global-mode "swift-playground-mode" nil t)
  )

(use-package haskell-mode
  :ensure t
  :init
  (add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
  (add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)
  (setq haskell-process-args-cabal-new-repl
        '("--ghc-options=-ferror-spans -fshow-loaded-modules"))
  (setq haskell-process-type 'cabal-new-repl)
  (setq haskell-stylish-on-save 't)
  (setq haskell-tags-on-save 't)
  )


(use-package flycheck-haskell
  :ensure t
  :config
  (add-hook 'flycheck-mode-hook #'flycheck-haskell-setup)
  (eval-after-load 'haskell-mode-hook 'flycheck-mode)
  )


(use-package flymake-hlint
  :ensure t
  :config
  (add-hook 'haskell-mode-hook 'flymake-hlint-load)
  )

(general-create-definer control-panel-leader
  :keymaps '(normal insert visual emacs)
  :prefix "SPC"
  :global-prefix "C-SPC")

(use-package hydra)

(control-panel-leader
  "t"  '(:ignore t :which-key "Toggles")
  "tt" '(counsel-load-theme :which-key "choose theme"))

(defhydra hydra-window-resize (:timeout 4)
  "scale text"
  ("<" evil-window-decrease-width "< increase")
  ("h" evil-window-decrease-width "< increase")
  (">" evil-window-increase-width "> decrease")
  ("l" evil-window-increase-width "> decrease")
  ("f" nil "finished" :exit t))

(control-panel-leader
  "tr" '(hydra-window-resize/body :which-key "resize window"))

(control-panel-leader
  "o" '(:ignore o :which-key "Org")
  "ob" '(:ignore o :which-key "babel")
  "obt" '(org-babel-tangle :which-key "tangle"))

(defhydra hydra-org-mode-subtree-promoting (:timeout 2)
  "Change Heading/Subtree level"
  (">" org-demote-subtree "< demote")
  ("l" org-demote-subtree "< demote")
  ("<" org-promote-subtree "< promote")
  ("h" org-promote-subtree "< promote")
  ("f" nil "finisher" :exit t))

(control-panel-leader
  "oh" '(hydra-org-mode-subtree-promoting/body :which-key "Change subtree level"))

(control-panel-leader
  "a" '(:ignore a :which-key "Apps")
  "ac" '(copilot-mode :which-key "Copilot")
  )

(control-panel-leader
  "r" '(:ignore r :which-key "Roam")
  "d" '(:ignore r :which-key "Dailies")
  "rf" '(org-roam-node-find :which-key "Find/Create Node")
  "ri" '(org-roam-node-insert :which-key "Isert Node")
  "rl" '(org-oram-buffer-toggle :which-key "Org Roam Buffer")
  "d" '(:ignore r :which-key "Daily")
  )

;; (setq panel-dailies-map (make-sparse-keymap))

;; (keymap-set panel-dailies-map "C-SPC d" org-roam-dailies-map)

(use-package git-auto-commit-mode
  :config
  (setq-default gac-automatically-push-p t)
  (setq-default gac-automatically-add-new-files-p t)

  (defun gac-pull-before-push (&rest _args)
    (let ((current-file (buffer-file-name)))
      (shell-command "git pull")
      (when current-file
        (with-current-buffer (find-buffer-visiting current-file)
          (revert-buffer t t t)))))
  (advice-add 'gac-push :before #'gac-pull-before-push)
  )


;; (keymap-set panel-dailies-map "C-SPC d" org-roam-dailies-map)

;; (control-panel-leader
;;   "a" '(:ignore a :which-key "apps")
;;   "ap" '(:ignore p :which-key "PlantUML rendering")
;;   "apa" '((setq plantuml-output-type "txt") :which-key "ASCII")
;;   "app" '((setq plantuml-output-type "png") :which-key "PNG")
;;   "aps" '((setq plantuml-output-type "svg") :which-key "SVG")
;;   )
