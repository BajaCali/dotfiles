alias v=nvim
alias vim=nvim

alias cd=cd ~
alias c='clear'
alias x=exit
alias q=exit

alias tr=tree -C

alias python=python3

alias school="cd /Users/michal/GDrive/School/FI/Bc/semestr_2"

alias project="cd ~/Projects/ios/big-nerd-ios-programming/ios"
