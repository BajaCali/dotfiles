# .dotfiles

This repo is used for storing my personal dotfiles.
I have used several frameworks to back it up (stew, mackup).
The future lies in custom one using few simple scripts involving `ln` command.

# Usage

## To restore on a new Mac

1. setup ssh
- [ ] crate ssh key: `ssh-keygen -t rsa -b 4096 -a 100`
- [ ] add the public part of key to GitLab (and GitHub if you want)

2. run setup script

```
cd ~
git clone git@gitlab.com:BajaCali/dotfiles.git
mv dotfiles .dotfiles
bash .dotfiles/setup.sh 
```

## To update a config

1. update the file as you normally would (i.e. not from .dotfiles directory)
2. go to the `~/.dotfiles` direcotory
3. commit & push with git

## To add new config file

Let `PATH` be path to config file or config folder you want to back up with out `~` or `/Users/michal/~ prefix.

1. Move your configs to `.dotfiles` folder with `mv ~/PATH ~/.dotfiles/PATH`
2. Make a symbolic link from the the location where the programme expects its configs with `ln -s ~/.dotfiles/PATH ~/PATH`
