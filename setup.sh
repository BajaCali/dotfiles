#!/bin/bash

NATIVES="tmux mackup tmuxinator neovim fd ag"
CASKS="brave-browser apptivate amethyst alacritty karabiner-elements spotify"

function execute() {
	msg_to_user=$1
	command=$2

	echo $msg_to_user
	echo "Press RETURN to continue or any other key to abort:"
	read -sn1 key

	if [[ $key = "" ]]; then
		eval "$command"
	else
		return
	fi
}

echo "Installing Homebrew"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/michal/.zprofile
    eval "$(/opt/homebrew/bin/brew shellenv)"

function enable_brew_multiuser() { # inspiration: https://stackoverflow.com/questions/41840479/how-to-use-homebrew-on-a-multi-user-macos-sierra-setup
	sudo dseditgroup -o edit -a $(whoami) -t user admin
	sudo chgrp -R admin $(brew --prefix)
	sudo chmod -R g+rwX $(brew --prefix)
	# ls -lah $(brew —prefix)
}
execute "Enable brew for all users?" "enable_brew_multiuser"

execute "Hide icons on Desktop?" "defaults write com.apple.finder CreateDesktop false; killall Finder"

execute "Install XCode Command Tools (no need to have XCode installed)?" "xcode-select --install"

execute "Install emacs?" "brew install emacs-plus --with-elrumo1-icon --with-imagemagick

# install native homebrew applications
for native in $NATIVES
do
	execute "Install ${native}?" "brew install $native"
done

# install homebrew cask applications
for cask in $CASKS
do
	execute "Install ${cask}?" "brew install --cask $cask"
done

function mackup_restore() {
	cp ~/.dotfiles/.mackup.cfg ~/
	cd ~
	mackup restore
}
execute "Restore via mackup (pulls dotfiles from GitLab and restores)?" "mackup_restore"

execute "Install SourceCodePro Powerline Awesome Regular font?" "open ~/.dotfiles/fonts/SourceCodePro+Powerline+Awesome+Regular.ttf"

execute "Install DejaVu Nerd font?" "open ~/.dotfiles/fonts/DejaVuSansMono/*.ttf"

execute "Install Nerd Symbols Only font?" "open ~/.dotfiles/fonts/NerdFontsSymbolsOnly/*.ttf"

function install_oh-my-zsh() { sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"; }

execute "Install Oh My ZSH?" "install_oh-my-zsh"

execute "Install Powerlevel10k Oh-my-zsh theme AND zsh-autsuggestions via git submodule?" "git submodule update --init --recursive"
